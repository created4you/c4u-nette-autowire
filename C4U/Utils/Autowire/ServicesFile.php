<?php

namespace C4U\Utils\Autowire;

use C4U\Utils\Autowire\Entity\DefaultEntity;
use C4U\Utils\Autowire\Entity\MapperEntity;
use Nette\Neon\Neon;

class ServicesFile {

	private $content;

	public function __construct(array $input) {
		$this->content = new \stdClass();
		$this->content->services = new \stdClass();

		$this->createBody($input);
	}

	private function createBody(array $input) {
		$output = array();
		foreach ($input as $row) {
			$type = $this->inferType($row->class);

			switch ($type) {
				case "Mapper": new MapperEntity($this->content->services, $row->class, $row->full); break;
				default: new DefaultEntity($this->content->services, $row->class, $row->full); break;
			}
		}
		return $output;
	}

	public function createNeon() {
		return $this->addHeader() . Neon::encode($this->content, Neon::BLOCK);
	}

	private function inferType($class) {
		if (preg_match('~Mapper$~', $class)) {
			return "Mapper";
		}

		return null;
	}

	private function addHeader() {
		return "# This file was generated automatically.\n# Do not modify directly! \n\n";
	}

}
