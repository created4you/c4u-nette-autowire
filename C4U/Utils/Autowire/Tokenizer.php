<?php

namespace C4U\Utils\Autowire;

class Tokenizer {

	public static function findClass($file) {
		$fp = fopen($file, 'r');
		$class = $namespace = $buffer = '';
		$i = 0;
		while (!$class) {
			if (feof($fp))
				break;

			$buffer .= fread($fp, filesize($file));
			$tokens = token_get_all($buffer);

			if (strpos($buffer, '{') === false)
				continue;

			for (; $i < count($tokens); $i++) {
				if ($tokens[$i][0] === T_NAMESPACE) {
					for ($j = $i + 1; $j < count($tokens); $j++) {
						if ($tokens[$j][0] === T_STRING) {
							$namespace .= '\\' . $tokens[$j][1];
						} else if ($tokens[$j] === '{' || $tokens[$j] === ';') {
							break;
						}
					}
				}

				if ($tokens[$i][0] === T_CLASS) {
					for ($j = $i + 1; $j < count($tokens); $j++) {
						if ($tokens[$j] === '{') {
							$class = $tokens[$i + 2][1];
						}
					}
				}
			}
		}

		if (!$class) return null;

		$output = new \stdClass();
		$output->class = $class;
		$output->namespace = $namespace;
		$output->full = ltrim($namespace . '\\' . $class, '\\');
		return $output;
	}

}
