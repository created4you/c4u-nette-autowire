<?php

namespace C4U\Utils\Autowire;

class Autowire {

	const AUTOWIRED_ANNOTATION = 'Autowired';
	const NO_ARGUMENT_ANNOTATION = 'NoArgument';
	const DIBI_CONNECTION_ANNOTATION = 'DibiConnection';

	public function run($path, $diFile) {
		// Check is dir.
		if (!is_dir($path))
			throw new \Exception('Dir: `' . $path . '` is not accessible.');

		$autowiredClass = $this->getAutowiredClass($path);

		// Create file.
		$this->createFile($autowiredClass, $diFile);
	}

	private function getAutowiredClass($path) {
		$output = array();

		// Create iterator.
		$iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
		$regexIterator = new \RegexIterator($iterator, '/^.+\.php$/i', \RecursiveRegexIterator::GET_MATCH);

		// Iterate.
		foreach ($regexIterator as $name => $object) {
			$class = Tokenizer::findClass($name);
			if ($class) {
				if (in_array(self::AUTOWIRED_ANNOTATION, $this->getClassAnnotations($class->full))) {
					$output[] = $class;
				}
			}

		}
		$this->sortAutowired($output);
		return $output;
	}

	public static function getClassAnnotations($class) {
		$r = new \ReflectionClass($class);
		$doc = $r->getDocComment();
		preg_match_all('#@(.*?)\n#s', $doc, $annotations);
		return array_map('self::trimValues', $annotations[1]);
	}

	public static function trimValues($value) {
		return trim($value);
	}

	private function sortAutowired(&$output) {
		usort($output, function($s1, $s2) {
			return strcmp($s1->class, $s2->class);
		});
	}

	private function createFile($autowiredClass, $diFile) {
		$servicesFile = new ServicesFile($autowiredClass);
		pr($servicesFile->createNeon());

		file_put_contents($diFile, $servicesFile->createNeon());
	}

}
