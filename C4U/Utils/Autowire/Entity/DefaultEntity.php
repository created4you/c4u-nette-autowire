<?php

namespace C4U\Utils\Autowire\Entity;

use C4U\Utils\Autowire\Autowire;
use Nette\Utils\Strings;

class DefaultEntity {

	public function __construct(\stdClass &$content, $className, $fullName) {
		$output = new \stdClass();
		$output->class = $fullName;
		if ($this->useArgument($fullName)) {
			$argument = $this->createArgument($className, $fullName);
			$output->arguments = is_array($argument) ? $argument : array($argument);
		}
		$content->{$this->createName($className)} = $output;
	}

	public function createName($name) {
		return lcfirst($name);
	}

	public function createArgument($name, $fullName = null) {
		$name = lcfirst($name);
		$name = ltrim(strtolower(preg_replace('/[A-Z]/', '_$0', $name)), '_');
		return $name;
	}

	private function useArgument($fullName) {
		return !in_array(Autowire::NO_ARGUMENT_ANNOTATION, Autowire::getClassAnnotations($fullName));
	}

}
