<?php

namespace C4U\Utils\Autowire\Entity;

use C4U\Utils\Autowire\Autowire;

class MapperEntity extends DefaultEntity {

	public function createArgument($name, $fullName = null) {
		$name = str_replace('Mapper', '', $name);
		$arguments = array();
		$arguments[] = parent::createArgument($name);
		$this->checkPresentDibiConnection($arguments, $fullName);
		return $arguments;
	}

	private function checkPresentDibiConnection(&$arguments, $fullName) {
		$allAnnotations = Autowire::getClassAnnotations($fullName);
		foreach ($allAnnotations as $annotation) {
			if (preg_match('~' . Autowire::DIBI_CONNECTION_ANNOTATION . '\(\'(.*)\'\)~', $annotation, $matches)) {
				if (count($matches) > 1) {
					$arguments[] = '@dibi.' . $matches[1];
				}
			}
		}
	}

}
